import { Component } from "react";
import { steps } from "./info";

class Content extends Component {
    render() {
        return (
            <div>
                <ul>
                    {
                        steps.map((item, index) => {
                            return (
                                <li key={index}>{item.id}. {item.title}: {item.content}</li>
                            )
                        })
                    }
                </ul>
            </div>
        )
    }
}

export default Content